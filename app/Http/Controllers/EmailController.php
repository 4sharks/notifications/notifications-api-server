<?php

namespace App\Http\Controllers;

use App\Mail\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public $fromName;
    public $fromAddress;
    public $sender;
    public $type;
    public $subject;
    public $template;
    public $user;
    public $payload;

    public function sendEmail(Request $request)
    {
        $this->sender = $request->input('sender');
        $this->type = $request->input('type');
        $this->user = $request->input('user');
        $this->payload = $request->input('payload');

        $this->template = 'emails.normal';
        $this->fromAddress = 'no-reply@absat.org';
        $this->fromName = 'أبسط أعمال Absat.org';

        if($this->type === 'NewRegister'){
            $this->subject = 'مرحبا بكم في أبسط أعمال';
            $this->template = 'emails.new-register';
        }

        $data = [
            "sender" => $this->sender,
            "from_address" => $this->fromAddress,
            "from_name" => $this->fromName,
            "template" =>  $this->template,
            "subject" => $this->subject,
            "user" => $this->user,
            "payload" => $this->payload

        ];

        if($this->sender){
            Mail::mailer('ses')->to($this->sender)->send(new SendEmail($data));
            return response()->json([
                "status" => 1,
                "message" => "success"
            ]);
        }

        return response()->json([
            "status" => 0,
            "message" => "error during send email"
        ]);
    }
}
