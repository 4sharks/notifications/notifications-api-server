<div dir="rtl" style="background-color:#281a62; color:#cccccc; padding:25px; border-radius:25px" >
    <div style="text-align: center; ">
        <img src="https://4sharks-absat.s3.eu-north-1.amazonaws.com/assets/absat-logo-dark.webp" style="max-width: 500px; background-color:#281a62;" />
    </div>
    <hr style="border: 1px solid #1c1245;"/>
    <div style="text-align: center "> عميلنا العزيز /   {{ @$data['user']['name']}}</div>
    <div style="font-weight: bold; padding:20px; text-align: center"> مرحبا بك في نظام أبسط أعمال </div>
    <div style="text-align: center">
        <span>يسعدنا انضمامك الي أبسط أعمال </span>
        <span>، حيث يمكنك الوصول الي جميع اعمالك عن طريق الرابط التالي</span>
    </div>
    <h2 style="text-align: center;  "><a href="https://apps.absat.org" target="_blank" style="text-decoration: none; color:#cccccc">apps.absat.org</a></h2>
    <div style="text-align: center; font-weight: bold">سجل الدخول الآن وقم بإدارة أعمالك</div>
    <div style="padding: 20px; text-align: center;">
        <div>اسم المستخدم الخاص بك هو </div>
        <div style="font-weight: bold"> {{ @$data['payload']['username'] }} </div>
    </div>
    <hr style="border: 1px solid #1c1245;"/>
    <div style="text-align:center; ">
        <div style="padding:5px">أبسط أعمال بسطناها عليك في كل شي</div>
        <div>www.absat.org</div>
    </div>
</div>

